# Описание задач - вариант 23

### Задача 1
```Подпункт 6```

Создать приложение, которое определяет в какой из двух 
целочисленных переменных больше цифр

---

### Задача 2
```Подпункт 6```

Составить список учебной группы, включающий Х человек. Для каждого студента указать: фамилию и имя, дату рождения (год, месяц и число), также у каждого студента есть зачетка, в которой указаны: предмет (от трех до пяти), дата экзамена, ФИО преподавателя.
Вывод виде таблицы ФИО всех студентов, у которых есть двойки